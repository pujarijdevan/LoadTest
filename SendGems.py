from itertools import cycle
import logging
from locust import HttpUser, task, between, SequentialTaskSet

USER_CREDENTIALS = [
    ("eleazertoluan", "LykaTesting"),
    ("reshu1611", "M@llTest!"),
    ('jagamich','Anujag0308$'),
    ('testdev06','testaccount')
]
USER_CACHE = []
USER_CACHE_CYCLE = cycle(USER_CACHE)


class UserBehaviour(SequentialTaskSet):

    def on_start(self):
        self.user_name = ''
        self.password = ''
        self.user_id = ''
        self.first_address_id = ''
        self.phone_number = ''
        self.base_header = dict()

        if len(USER_CREDENTIALS) > 0:
            self.user_name, self.password = USER_CREDENTIALS.pop()

            form_data = {
                "countryCode": "PH",
                "username": self.user_name,
                "password": self.password,
                "device": {
                    "deviceId": "CD355C33-2F68-4975-B292-B623C08F660F",
                    "deviceOs": "iOS",
                    "deviceModel": "iPhone 11 Pro Max",
                    "osVersion": "14.0",
                    "deviceName": "iOS"
                }
            }

            self.base_header = {
                'Connection': 'keep-alive',
                'deviceOs': 'ios',
                'Content-Type': 'application/json',
                'Request-Origin-App': 'Lyka-App',
                'User-Agent': 'LykaShopDemo/1 CFNetwork/1121.2.2 Darwin/19.2.0'
            }
            with self.client.post("https://identity-awsdev.mylykaapps.com/useraccounts/login", json=form_data,
                                  headers=self.base_header,
                                  catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed, EXCEPTION: " + response.text)
                self.base_header['Authorization'] = "Bearer " + response.json()['data']['token']['accessToken']
                self.user_id = response.json()['data']['user']['id']
                logging.info('Login with %s email and %s password', self.user_name, self.password)

            with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/profiles/GetUserProfileForEditing",
                                 headers=self.base_header,
                                 catch_response=True) as response1:
                if response1.status_code != 200:
                    response1.failure("Failed to fetch the user profile, EXCEPTION: " + response1.text)


            USER_CACHE.append({
                'user_name': self.user_name,
                'password': self.password,
                'base_header': self.base_header,
                'user_id': self.user_id,
                'user_first_address_id': self.first_address_id,
                'phone_number': self.phone_number,
            })

    @task
    def send_gem(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if not USER_CREDENTIALS and USER_CACHE:
            data = next(USER_CACHE_CYCLE)
            self.user_name = data['user_name']
            self.password = data['password']
            self.base_header = data['base_header']
            self.user_id = data['user_id']
            self.first_address_id = data['user_first_address_id']
            self.phone_number = data['phone_number']

        if self.user_name:
            print("User id in task 3", self.user_id)
            print("User name in task 3", self.user_name)
            print("Phone number in task 3", self.phone_number)
            print("Address Id in task 3", self.first_address_id)
            form_data = {
                "device": {
                    "deviceId": "6C672896-3ADC-42E7-9540-110A9D3EBA6D",
                    "deviceModel": "iPhone XS",
                    "deviceOs": "12.0",
                    "osVersion": "iOS",
                    "deviceName": "iOS",
                    "isEmulator": False
                },
                "amount": 1,
                "recipientId": 55,
                "passcode": 1111
            }

            with self.client.post("https://gateway-awsdev.mylykaapps.com/api/v3/wallets/SendGemV2", headers=self.base_header,
                                  json=form_data,
                                  catch_response=True) as response:
                print("Printing responsme")
                print(response.status_code)
                print("Response",response.json)
                if response.status_code != 200:
                    response.failure("Failed to send Gem, EXCEPTION: " + response.text)
                    response.failure(response.status_code)


class User(HttpUser):
    tasks = [UserBehaviour]
    wait_time = between(1, 2)