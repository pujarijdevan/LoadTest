from random import choice
from locust import HttpUser, task, between, SequentialTaskSet

USER_CREDENTIALS = [
    ("eleazertoluan", "LykaTesting"),
    ("kale_10", "TestPass123"),
]
USER_CACHE = []


class UserBehaviour(SequentialTaskSet):

    def on_start(self):
        self.user_name = ''
        self.password = ''
        self.user_id = ''
        self.request_id = ''
        self.base_header = dict()

        if len(USER_CREDENTIALS) > 0:
            self.user_name, self.password = USER_CREDENTIALS.pop()
           # logging.info('Login with %s user_name and %s password', self.user_name, self.password)
            form_data = {
                "countryCode": "PH",
                "username": self.user_name,
                "password": self.password,
                "device": {
                    "deviceId": "CD355C33-2F68-4975-B292-B623C08F660F",
                    "deviceOs": "iOS",
                    "deviceModel": "iPhone 11 Pro Max",
                    "osVersion": "14.0",
                    "deviceName": "iOS"
                }
            }

            self.base_header = {
                'Connection': 'keep-alive',
                'deviceOs': 'ios',
                'Content-Type': 'application/json',
                'Request-Origin-App': 'Lyka-App',
                'User-Agent': 'LYKA_POSTMAN'
            }
            with self.client.post("https://identity-awsdev.mylykaapps.com/useraccounts/login", json=form_data,
                                  headers=self.base_header,
                                  catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed, EXCEPTION: " + response.text)
                self.base_header['Authorization'] = "Bearer " + response.json()['data']['token']['accessToken']
                self.user_id = response.json()['data']['user']['id']

                with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/profiles/GetUserProfileForEditing",
                                     headers=self.base_header,
                                     catch_response=True) as response1:
                    if response1.status_code != 200:
                        response1.failure("Failed to fetch the user profile, EXCEPTION: " + response1.text)

                form_data2={
                "device": {
                    "deviceId": "6C672896-3ADC-42E7-9540-110A9D3EBA6D",
                    "deviceModel": "iPhone XS",
                    "deviceOs": "12.0",
                    "osVersion": "iOS",
                    "deviceName": "iOS",
                    "isEmulator": False
                },
                "amount": 1,
                "recipientId": 700000117558,
                "passcode": 1111
            }
           # with self.client.post("https://gateway-awsdev.mylykaapps.com/api/v3/wallets/RequestSendGem",
                                 # headers=self.base_header, json=form_data,
                                 # catch_response=True) as response:
                #print(response)
               # if response.status_code != 200:
                 #   response.failure("Failed to fetch the refrence-id, EXCEPTION: " + response.text)
                #self.request_id = response.json()['data']

            USER_CACHE.append({
                'user_name': self.user_name,
                'password': self.password,
                'base_header': self.base_header,
                'user_id': self.user_id,
                #'request_id' : self.request_id
            })

    @task
    def getGems(self):
         # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if not USER_CREDENTIALS and USER_CACHE:
            data = choice(USER_CACHE)
            self.user_name = data['user_name']
            self.password = data['password']
            self.base_header = data['base_header']



            with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/wallets/GetGems",
                                 headers=self.base_header,
                                 catch_response=True) as response:
                      if response.status_code != 200:
                          response.failure("Failed to fetch the user balance, EXCEPTION: " + response.text)
                          self.balance = response.json()['data']['totalGem']


    @task
    def getTransactionalHistory(self):
         # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if not USER_CREDENTIALS and USER_CACHE:
            data = choice(USER_CACHE)
            self.user_name = data['user_name']
            self.password = data['password']
            self.base_header = data['base_header']




            with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/wallets/GetTransactionHistory?pageSize=10&pageIndex=1",
                                 headers=self.base_header,
                                 catch_response=True) as response:
                      if response.status_code != 200:
                          response.failure("Failed to fetch the transaction, EXCEPTION: " + response.text)
                          response.failure("Statuscode",response.status_code)




    #@task
    def send_gems(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
            if not USER_CREDENTIALS and USER_CACHE:
                    data = choice(USER_CACHE)
                    self.user_name = data['user_name']
                    self.password = data['password']
                    self.base_header = data['base_header']
                    self.request_id = data['request_id']

                    form_data={
                    "device": {
                        "deviceId": "6C672896-3ADC-42E7-9540-110A9D3EBA6D",
                        "deviceModel": "iPhone XS",
                        "deviceOs": "12.0",
                        "osVersion": "iOS",
                        "deviceName": "iOS",
                        "isEmulator": False
                    },
                    "amount": 1,
                    "recipientId": 700000117558,
                    "passcode": 1111,
                    "requestId": str(self.request_id),
                    "countryCode": "PH"
                }

                    with self.client.post("https://gateway-awsdev.mylykaapps.com/api/v3/wallets/SendGemV3",
                                         headers=self.base_header,  json=form_data,
                                         catch_response=True) as response:
                        if response.status_code != 200:
                            response.failure("Failed to fetch the refrence-id, EXCEPTION: " + response.text)
                            print(response.json)


    @task
    def get_badge(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
            if not USER_CREDENTIALS and USER_CACHE:
                    data = choice(USER_CACHE)
                    self.user_name = data['user_name']
                    self.password = data['password']
                    self.base_header = data['base_header']
                    self.user_id = data['user_id']

                    with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/users/GetUserBadge?id=" + str(self.user_id),
                                         headers=self.base_header,
                                         catch_response=True) as response:
                        if response.status_code != 200:
                            response.failure("Failed to fetch the response, EXCEPTION: " + response.text)
                            print(response)

    @task
    def get_highest_Ranking(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
            if not USER_CREDENTIALS and USER_CACHE:
                    data = choice(USER_CACHE)
                    self.user_name = data['user_name']
                    self.password = data['password']
                    self.base_header = data['base_header']
                    self.user_id = data['user_id']

                    with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/Rankings/GetTopGemLocalLeaders?type=user&countryCode=PH&pageIndex=1&pageSize=100",
                                         headers=self.base_header,
                                         catch_response=True) as response:
                        if response.status_code != 200:
                            response.failure("Failed to fetch the response, EXCEPTION: " + response.text)
                            print(response)


class User(HttpUser):
    tasks = [UserBehaviour]
    wait_time = between(1, 2)