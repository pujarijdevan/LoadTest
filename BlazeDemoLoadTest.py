import datetime
import re
import time
from locust import HttpUser, TaskSet, task, between


class BrowseDocumentation(HttpUser):
    @task
    def index_page_with_response_duration_assertion(self):
        with self.client.get("/", catch_response=True) as response:
            if response.status_code == 200 and response.text == "Success":
                print(response.status_code)
                response.success()
            else:
                response.failure("Response code is" + str(response.status_code))

            if response.elapsed.total_seconds() < 2:
                response.success()
            else:
                response.failure("Response took more time" + str(response.elapsed.total_seconds()))
