**Tech Stack used :**

  

  

- Locust

- Python

  

  

**Steps to run load test :**

  

  

- [Install Python](https://docs.python-guide.org/starting/installation/) 3.6 or later, if you don't already have it.

  

  

- Install Locust:

  

$ pip3 install locust

  

Validate your installation. If this doesn't work, [check the wiki](https://github.com/locustio/locust/wiki/Installation) for some possible solutions.

  

$ locust -V

  

locust 2.2.3

  

**Steps to run**

  clone the repo using git clone https://git.toptal.com/screening/Pujari-Devan.git 

1. Navigate to LoadTest folder -  cd LoadTest

2. Run the command locust -f BlazeDemoLoadTest.py which displays a webinterface hosted in http://0.0.0.0:8089

3. open http://0.0.0.0:8089

4. Enter the HOST as http://blazedemo.com and Number of Users 1000 and Spawn Rate : 100 and click on start swarming

  

You can run for 15 seconds and see graphs and downlaod result .

  

To run in command line you can run the command in command line

locust -f BlazeDemoLoadTest.py --host=https://blazedemo.com/ -u 1000 -r 100 -t 15s --headless --html=report.html
