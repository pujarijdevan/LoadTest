curl --location --request POST 'https://gateway-awsdev.mylykaapps.com/api/v3/wallets/SendGemV2' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjYmMyMTA1NGY0MDczYThjNzRmMTFiYzQ5ZmM0OTMxIiwidHlwIjoiYXQrand0In0.eyJuYmYiOjE2MzU4NTE3NjYsImV4cCI6MTYzODQ0Mzc2NiwiaXNzIjoiaHR0cHM6Ly9pZGVudGl0eS1hd3NkZXYubXlseWthYXBwcy5jb20iLCJhdWQiOiJhcGkxIiwiY2xpZW50X2lkIjoicm8uY2xpZW50Iiwic3ViIjoiYmMxZmYzZTctYmRlYy00MWM1LTgxMDMtNTVhOTc1N2MzZDBlIiwiYXV0aF90aW1lIjoxNjM1ODUxNzY2LCJpZHAiOiJsb2NhbCIsIm5hbWUiOiJqYWdhbWljaCIsInVzZXJuYW1lIjoiamFnYW1pY2giLCJpZCI6IjcwMDAwMDExNTgxNCIsImp0aSI6Im5xVGdJZHJIQkV4aWNLWHdhV3J0b1EiLCJzY29wZSI6WyJhcGkxIiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbInB3ZCJdfQ.lMVaZJ1J_3FRYngGxRt7JvaCDjPVQtJdhe00Fj9A5UGxnqj33Al_XDZ_z5Xl0v2hoxv5XCBAgzDjgLrujupFSintdO6mmTPiurolBa2fTrA0adpxhM8Qx8VA0uX69TUTDU_sG94AGHPCKbMd11QeNzhD-Vob608qRrKFa-vQYQDKrJNvB2PIJH6iVvPkl2JQvbmI6zxS_iBWvH93ObuSOCuM48wcbdDGfgdkf0-NIC7jMA6n8WXBLUTrZsvPD-AYJmAAYHkX2oKb3rkmGWP500AiBD0Xi-i4Yn17FREHc_RW45SUghPN9yyyFIXeDMH_6o8Y0kqSzwv_2vAkdG3ogQ' \
--data-raw '{
	"device": {
		"deviceId": "6C672896-3ADC-42E7-9540-110A9D3EBA6D",
        "deviceModel": "iPhone XS",
        "deviceOs": "12.0",
        "osVersion": "iOS",
        "deviceName": "iOS",
        "isEmulator": false
	},
	"amount": 1,
    "recipientId": 55,
    "passcode": 1111
}'